
#ifndef __PES_PACKET_H__
#define __PES_PACKET_H__

#include <memory>
#include <vector>
#include <cstdint>

class PesPacket
{
	uint16_t _pid;
	bool _random_access;
	std::vector<uint8_t> _data;
public:

	PesPacket(uint16_t pid, bool ra, std::vector<uint8_t> && data);

	uint16_t pid() const;
	uint64_t dts() const;
	uint64_t pts() const;
	bool random_access() const;
	const std::vector<uint8_t> & data() const;
};

typedef std::shared_ptr<PesPacket> PesPacketPtr;

#endif /*__PES_PACKET_H__*/
