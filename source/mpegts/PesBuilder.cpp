
#include <algorithm>
#include <cassert>
#include <bitstream/mpeg/pes.h>
#include <bitstream/mpeg/ts.h>
#include <utils.h>
#include "PesBuilder.h"

PesBuilder::PesBuilder(int input_index, uint16_t pid)
{
	_pid = pid;
	auto log_name = fmt::format("pesb:{:02d}:{:04d}", input_index, pid);
	_log = spdlog::stdout_logger_mt(log_name);
}

std::array<PesPacketPtr,2> PesBuilder::build(uint8_t pkt[188])
{
	assert(ts_get_pid(pkt) == _pid);

	SPDLOG_TRACE(_log, "added packet pusi: {} ra: {} collected_payload_size: {}", ts_get_unitstart(pkt), ts_has_adaptation(pkt) && ts_get_adaptation(pkt) > 0 ? tsaf_has_randomaccess(pkt): 0, _payload.size());

	std::array<PesPacketPtr,2> ret;
	std::fill(ret.begin(), ret.end(), nullptr);
	size_t ret_cnt = 0;

	if(!_unitstart_seen && !ts_get_unitstart(pkt))
	{
		_log->warn("skipping ts packet until first unitstart");
		return ret;
	}

	if(_last_cc != uint8_t(-1) && ts_check_discontinuity(ts_get_cc(pkt), _last_cc))
	{
		_log->warn("discontinuity found: last cc: {} current: {}", _last_cc, ts_get_cc(pkt));
		_last_cc = ts_get_cc(pkt);
	}

	if(ts_get_unitstart(pkt))
	{
		_unitstart_seen = true;

		if(_payload.size() > 0)
		{
			ret[ret_cnt] = _flush_pes();
		}

		if(ts_has_adaptation(pkt) && ts_get_adaptation(pkt) > 0)
		{
			if(tsaf_has_randomaccess(pkt))
			{
				_random_access_flag = true;
			}
		}
	}

	if(ts_has_payload(pkt))
	{
		auto p = ts_payload(pkt);
		auto p_size = pkt + 188 - p;

		_payload.insert(_payload.end(), p, p + p_size);
	}

	if(_payload.size() >= PES_HEADER_SIZE)
	{
		auto pes_size = pes_get_length(_payload.data());
		pes_size += pes_size ? PES_HEADER_SIZE : 0;

		if(pes_size != 0 && _payload.size() >= pes_size)
		{
			ret[ret_cnt] = _flush_pes();
		}
	}

	return ret;
}

PesPacketPtr PesBuilder::_flush_pes()
{
	assert(_payload.size() > 0);

	size_t pes_size = 0;

	if(_payload.size() >= PES_HEADER_SIZE)
	{
		pes_size = pes_get_length(_payload.data());
		pes_size += pes_size ? PES_HEADER_SIZE : 0;
	}

	if(pes_size != 0 && _payload.size() != pes_size)
	{
		_log->warn("found broken pes packet: size mismatch: collected {} bytes but pes should be of {} bytes ({} bytes diff)", _payload.size(), pes_size, int(pes_size - _payload.size()));
	}

	if((pes_size != 0 && _payload.size() != pes_size) || ! pes_validate(_payload.data()) || ! pes_validate_header(_payload.data()))
	{
		_log->warn("invalid pes packet header: {}", dump_bytes(_payload));
	}

	auto pes = std::make_shared<PesPacket>(_pid, _random_access_flag, std::move(_payload));
	_random_access_flag = false;

	assert(_payload.size() == 0);

	return pes;
}
