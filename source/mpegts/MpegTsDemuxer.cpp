
#include <bento4/Ap4AdtsParser.h>
#include <bitstream/mpeg/aac.h>
#include <bitstream/mpeg/ts.h>
#include <bitstream/mpeg/pes.h>
#include <bitstream/mpeg/psi.h>
#include <bitstream/mpeg/psi/pat.h>
#include <bitstream/mpeg/psi/pmt.h>
#include <spdlog/spdlog.h>
#include <enforce.h>
#include "MpegTsDemuxer.h"

MpegTsDemuxer::MpegTsDemuxer(ByteReaderPtr bs, int input_index)
{
	_bs = bs;
	_state = State::BUILD_PAT;
	_log = spdlog::stdout_logger_mt("mpegts");
	_input_index = input_index;
	_last_dts = TimeValue(0, 1);

	psi_table_init(_pat);

	_packet_handler[State::BUILD_PAT] = std::bind(&MpegTsDemuxer::_build_pat, this, std::placeholders::_1);
	_packet_handler[State::BUILD_PMT] = std::bind(&MpegTsDemuxer::_build_pmt, this, std::placeholders::_1);
	_packet_handler[State::BUILD_AU]  = std::bind(&MpegTsDemuxer::_build_au, this, std::placeholders::_1);
}

MpegTsDemuxer::~MpegTsDemuxer()
{
	psi_table_free(_pat);
}

bool MpegTsDemuxer::initialized() const
{
	return _state == State::BUILD_AU;
}

bool MpegTsDemuxer::eof() const
{
	return _bs->eof();
}

TimeValue MpegTsDemuxer::last_dts()
{
	return _last_dts;
}

const std::vector<Stream> & MpegTsDemuxer::streams() const
{
	return _streams;
}

AccessUnitPtr MpegTsDemuxer::demux()
{
	while(_au_list.empty() && _consume_some_data())
	{
		;
	}

	AccessUnitPtr au;

	if(_au_list.size() > 0)
	{
		au = _au_list.front();
		_au_list.pop_front();
		_last_dts = au->dts();
	}

	return au;
}

bool MpegTsDemuxer::_consume_some_data()
{
	uint8_t buf[188];

	auto sz = _bs->read(buf, 188);

	if(sz > 0)
	{
		ENFORCE(sz == 188);
		ENFORCE(buf[0] == 0x47);
		_consume_packet(buf);
	}

	return sz > 0;
}

void MpegTsDemuxer::_consume_packet(uint8_t pkt[])
{
	_packet_handler[_state](pkt);
}

void MpegTsDemuxer::_build_pat(uint8_t pkt[])
{
	auto pid = ts_get_pid(pkt);
	if(pid != 0)
	{
		return;
	}

	uint8_t * psi_buffer;
	uint16_t psi_buffer_used = 0;

	psi_assemble_init(&psi_buffer, &psi_buffer_used);
	const uint8_t * payload = ts_payload(pkt);
	payload += *payload + 1;
	uint8_t payload_size = pkt + 188 - payload;

	uint8_t * section = psi_assemble_payload(&psi_buffer, &psi_buffer_used, &payload, &payload_size);

	if(section == NULL
		|| ! psi_validate(section)
		|| ! psi_check_crc(section))
	{
		_log->error("failed to build pat section");
		psi_assemble_reset(&psi_buffer, &psi_buffer_used);
		return;
	}

	psi_table_section(_pat, section);
	psi_assemble_reset(&psi_buffer, &psi_buffer_used);

	_state = State::BUILD_PMT;
}

void MpegTsDemuxer::_build_pmt(uint8_t pkt[])
{
	auto prog0 = pat_get_program(_pat[0], 0);

	if(ts_get_pid(pkt) != patn_get_pid(prog0))
	{
		return;
	}

	uint8_t * psi_buffer;
	uint16_t psi_buffer_used = 0;

	psi_assemble_init(&psi_buffer, &psi_buffer_used);
	const uint8_t * payload = ts_payload(pkt);
	payload += *payload + 1;
	uint8_t payload_size = pkt + 188 - payload;

	uint8_t * section = psi_assemble_payload(&psi_buffer, &psi_buffer_used, &payload, &payload_size);

	if(section == NULL
		|| ! psi_validate(section)
		|| ! psi_check_crc(section)
		|| ! pmt_validate(section))
	{
		_log->error("failed to build pmt section");
		psi_assemble_reset(&psi_buffer, &psi_buffer_used);
		return;
	}

	psi_assemble_reset(&psi_buffer, &psi_buffer_used);

	for(int esid = 0; ; ++esid)
	{
		auto es = pmt_get_es(section, esid);
		if(es == nullptr)
		{
			break;
		}

		auto pid = pmtn_get_pid(es);
		int sid = pmtn_get_streamtype(es);

		_log->info("found stream 0x{:02x} ({:d}) at input {} pid {}: {}", sid, sid, _input_index, pid, pmt_get_streamtype_txt(sid));
		auto codec_id = stream_id_to_codec_id(sid);
		auto pesb = std::make_shared<PesBuilder>(_input_index, pid);
		_pes_builders[pid] = pesb;

		Stream s;

		s.input = _input_index;
		s.index = _streams.size();
		s.codec_id = codec_id;

		_streams.push_back(s);
		_pid_to_stream[pid] = s.index;
	}

	_state = State::BUILD_AU;
}

void MpegTsDemuxer::_build_au(uint8_t pkt[])
{
	auto pid = ts_get_pid(pkt);

	if(_pes_builders.count(pid) == 0)
	{
		return;
	}

	auto pesb = _pes_builders[pid];

	auto pes_arr = pesb->build(pkt);

	for(auto pes: pes_arr)
	{
		if(pes)
		{
			SPDLOG_DEBUG(_log, "pes created: pid: {} dts: {} pts: {}", pes->pid(), pes->dts(), pes->pts());
			_split_pes(pes);
		}
	}
}

void MpegTsDemuxer::_split_pes(PesPacketPtr pes)
{
	auto sid = _pid_to_stream[pes->pid()];
	auto & s = _streams[sid];

	switch(s.codec_id)
	{
		case CodecId::H264:
			_split_null(s, pes);
			break;
		case CodecId::Aac:
			_split_aac(s, pes);
			break;
		case CodecId::Ac3:
			_split_ac3(s, pes);
			break;
		default:
			_split_null(s, pes);
			break;
	}
}

void MpegTsDemuxer::_split_null(const Stream & s, PesPacketPtr pes)
{
	const uint8_t* p = pes_payload((uint8_t*) pes->data().data());

	std::vector<uint8_t> payload(p, p + pes->data().size());

	auto au = std::make_shared<AccessUnit>(s, pes->random_access(), TimeValue(pes->dts(), 90000), TimeValue(pes->pts(), 90000), Optional<Duration>(), std::move(payload));

	_au_list.push_back(au);
}

void MpegTsDemuxer::_split_aac(const Stream & s, PesPacketPtr pes)
{
	const uint8_t* payload = pes_payload((uint8_t*) pes->data().data());
	size_t payload_size = pes->data().data() + pes->data().size() - payload;
	auto payload_end = payload + payload_size;

	ENFORCE(pes->pts() == pes->dts());

	auto next_adts_frame_time = TimeValue(pes->dts(), 90000);

	while(payload + 7 < payload_end)
	{
		// check syncword 0xfff at start of each adts frame
		ENFORCE(payload[0] == 0xff);
		ENFORCE((payload[1] & 0xf0) == 0xf0);

		auto frame_length = adts_get_length(payload);
		auto sampling_frequency_idx = adts_get_sampling_freq(payload);
		auto sampling_frequency = AP4_AdtsSamplingFrequencyTable[sampling_frequency_idx];
		auto frame_duration = Duration(1024, sampling_frequency);

		std::vector<uint8_t> frame_data(payload, payload + frame_length);

		auto au = std::make_shared<AccessUnit>(s, true, next_adts_frame_time, next_adts_frame_time, frame_duration, std::move(frame_data));

		_au_list.push_back(au);

		next_adts_frame_time += frame_duration;
		payload += frame_length;
	}

	ENFORCE(payload == payload_end);
}

void MpegTsDemuxer::_split_ac3(const Stream & s, PesPacketPtr pes)
{
	throw std::runtime_error("mpegts: ac3 codec is not yet implemented");
}

void MpegTsDemuxer::probe()
{
	uint8_t buf[188];

	while(!initialized() && _bs->read(buf, 188) == 188)
	{
		_consume_packet(buf);
	}
}
