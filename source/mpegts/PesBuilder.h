
#ifndef __PES_BUILDER_H__
#define __PES_BUILDER_H__

#include <array>
#include <vector>
#include <spdlog/spdlog.h>
#include "PesPacket.h"

class PesBuilder
{
	uint16_t _pid;
	bool _unitstart_seen = false;
	std::vector<uint8_t> _payload;
	bool _random_access_flag = false;
	std::shared_ptr<spdlog::logger> _log;
	uint8_t _last_cc = -1;

	PesPacketPtr _flush_pes();
public:

	PesBuilder(int inpit_index, uint16_t pid);

	std::array<PesPacketPtr,2> build(uint8_t pkt[188]);
};

typedef std::shared_ptr<PesBuilder> PesBuilderPtr;

#endif /*__PES_BUILDER_H__*/
