
#include "FileByteReader.h"

FileByteReader::FileByteReader(std::string path)
{
	_fd = fopen(path.c_str(), "rb");
}

FileByteReader::~FileByteReader()
{
	fclose(_fd);
}

size_t FileByteReader::read(uint8_t * buf, size_t sz)
{
	return fread(buf, 1, sz, _fd);
}

bool FileByteReader::eof() const
{
	return feof(_fd);
}
