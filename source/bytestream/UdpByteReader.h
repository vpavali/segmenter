
#pragma once

#include <cstdint>
#include <deque>
#include <string>
#include "ByteReader.h"

class UdpByteReader: public ByteReader
{
	int _fd;
	time_t _last_packet_time;
	std::deque<uint8_t> _buffer;
	std::shared_ptr<spdlog::logger> _log;

	void _try_to_fill_buffer();
public:
	UdpByteReader(std::string group, uint16_t port);
	~UdpByteReader();

	size_t read(uint8_t* buf, size_t size) override;
	bool eof() const override;
};
