
#ifndef FILE_BYTE_READER_H
#define FILE_BYTE_READER_H

#include <string>
#include <cstdio>
#include <bytestream/ByteReader.h>

class FileByteReader : public ByteReader
{
	FILE * _fd;
public:

	FileByteReader(std::string path);
	~FileByteReader();

	size_t read(uint8_t* buf, size_t size) override;
	// bool would_block() const override;
	bool eof() const override;
};

#endif
