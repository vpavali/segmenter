
#ifndef DEMUXER_H
#define DEMUXER_H

#include <memory>
#include <AccessUnit.h>
#include <time.h>

class Demuxer
{
public:

	virtual AccessUnitPtr demux() = 0;
	virtual void probe() = 0;
	virtual TimeValue last_dts() = 0;
	virtual bool initialized() const = 0;
	virtual bool eof() const = 0;
	virtual const std::vector<Stream> & streams() const = 0;

	virtual ~Demuxer(){}
};

typedef std::shared_ptr<Demuxer> DemuxerPtr;

#endif
