
#include <bento4/Ap4AdtsParser.h>
#include <bento4/Ap4AvcParser.h>
#include <bitstream/mpeg/aac.h>
#include <bitstream/mpeg/h264.h>
#include <h264bs/h264_stream.h>
#include <utils.h>
#include "Parser.h"

using json11::Json;

FullParseInfo Parser::parse(MultiTrackSegmentPtr mts)
{
	FullParseInfo fpi;

	for(auto ms: mts->segments())
	{
		auto track_info = _parse(ms);
		fpi.push_back(track_info);
	}

	return fpi;
}

TrackParseInfo Parser::_parse(MultiSegmentPtr ms)
{
	TrackParseInfo ret;

	for(auto s: ms->segments())
	{
		auto pi = _parse(s);
		ret.push_back(pi);
	}

	return ret;
}

ParseInfoPtr Parser::_parse(const MonoSegment & ms)
{
	switch(ms.stream().codec_id)
	{
		case CodecId::H264:
			return _parse_h264(ms);
		case CodecId::Aac:
			return _parse_aac(ms);
		case CodecId::Ac3:
			return _parse_ac3(ms);
		default:
			throw std::runtime_error(fmt::format("unknown codec {}", (int) ms.stream().codec_id));
	}
}

ParseInfoPtr Parser::_parse_ac3(const MonoSegment & ms)
{
	NOT_IMPLEMENTED()
}

void Parser::_calc_bitrate(ParseInfoPtr pi, const MonoSegment & ms)
{
	size_t total_size_bytes = 0;

	for(auto au: ms.samples)
	{
		total_size_bytes += au->data().size();
	}

	pi->average_bitrate = total_size_bytes * 8.0 / rescale_double(ms.duration, 1);
	pi->max_bitrate = pi->average_bitrate;
}

ParseInfoPtr Parser::_parse_h264(const MonoSegment & ms)
{
	auto pi = std::make_shared<H264ParseInfo>();

	_calc_bitrate(pi, ms);

	pi->stream = ms.stream();

	AP4_AvcNalParser parser;

	auto au = ms.samples[0];
	const AP4_DataBuffer * nalubuf = NULL;
	size_t offset = 0;

	// printf("au codec: %d data: %s\n", au->stream().codec_id, dump_bytes(au->data().begin(), au->data().end()).c_str());

	while(offset < au->data().size())
	{
		AP4_Size bytes_consumed = 0;

		auto res = parser.Feed(au->data().data() + offset, au->data().size() - offset, bytes_consumed, nalubuf, true);

		ENFORCE(res == 0 && bytes_consumed > 0 && nalubuf != nullptr);

		int nalutype = nalubuf->GetData()[0] & 0x1f;

		// auto s = dump_bytes(nalubuf->GetData(), nalubuf->GetData() + nalubuf->GetDataSize());
		// printf("nalutype: %d nalu: %s\n", nalutype, s.c_str());

		switch(nalutype)
		{
		case H264NAL_TYPE_SPS:
			{
				auto h = h264_new();
				auto sz = read_nal_unit(h, (uint8_t*) nalubuf->GetData(), nalubuf->GetDataSize());

				ENFORCE(sz == nalubuf->GetDataSize());
				ENFORCE(h->sps != nullptr);

				auto sps = (const uint8_t *) nalubuf->GetData() + 1;

				pi->width = h->sps->pic_width_in_mbs_minus1 * 16 + 16;
				pi->height = h->sps->pic_height_in_map_units_minus1 * 16 + 16;
				pi->profile = sps[0];
				pi->profile_compatibility = sps[1];
				pi->level = sps[2];
				pi->sps_list.emplace_back(nalubuf->GetData(), nalubuf->GetData() + nalubuf->GetDataSize());

				h264_free(h);

				spdlog::get("app")->debug("h264: width: {} height: {} profile: {} level: {} profile_compatibility: {}", pi->width, pi->height, pi->profile, pi->level, pi->profile_compatibility);
			}
			break;
		case H264NAL_TYPE_PPS:
			pi->pps_list.emplace_back(nalubuf->GetData(), nalubuf->GetData() + nalubuf->GetDataSize());
			break;
		}

		offset += bytes_consumed;
	}

	return pi;
}

ParseInfoPtr Parser::_parse_aac(const MonoSegment & ms)
{
	auto pi = std::make_shared<AacParseInfo>();

	_calc_bitrate(pi, ms);

	pi->stream = ms.stream();

	const uint8_t * adts_frame = ms.samples[0]->data().data();

	auto sampling_frequency_idx = adts_get_sampling_freq(adts_frame);
	pi->sampling_frequency_index = sampling_frequency_idx;
	pi->sampling_rate = AP4_AdtsSamplingFrequencyTable[sampling_frequency_idx];
	auto channels_configuration = adts_get_channels(adts_frame);


	static const uint8_t channels_map[] = {
		0,1,2,3,4,5,6,8
	};

	static const char * channels_configuration_map[] = {
		"<unknown>","1","2","3","4","5","5.1","7.1"
	};

	ENFORCE(channels_configuration > 0);
	ENFORCE(channels_configuration < 8);

	pi->channels = channels_map[channels_configuration];
	pi->channels_configuration = channels_configuration;
	pi->channels_configuration_str = channels_configuration_map[channels_configuration];

	return pi;
}

std::string AacParseInfo::codec_private_data() const
{
	return "";
}

Json AacParseInfo::quality_info()
{
	Json bitrate = Json::object {
		{"max", max_bitrate},
		{"average", average_bitrate},
	};

	Json qi = Json::object {
		{"quality_id", average_bitrate},
		{"bitrate", bitrate},
		{"codec_private_data", codec_private_data()},

		{"codec", "aac"},
		{"sampling_rate", sampling_rate},
		{"sample_bit_depth", sample_bit_depth},
		{"channels", channels},
		{"channels_configuration", channels_configuration},
	};

	return qi;
}

std::string H264ParseInfo::codec_private_data() const
{
	std::ostringstream os;

	for(auto & sps: sps_list)
	{
		os << "00000001";
		for(auto x: sps)
		{
			os << fmt::format("{:02x}", x);
		}
	}

	for(auto & pps: pps_list)
	{
		os << "00000001";
		for(auto x: pps)
		{
			os << fmt::format("{:02x}", x);
		}
	}

	return os.str();
}

Json H264ParseInfo::quality_info()
{
	Json bitrate = Json::object {
		{"max", max_bitrate},
		{"average", average_bitrate},
	};

	Json qi = Json::object {
		{"quality_id", average_bitrate},
		{"bitrate", bitrate},
		{"codec_private_data", codec_private_data()},

		{"codec", "h264"},
		{"width", width},
		{"height", height},
		{"profile", profile},
		{"profile_compatibility", profile_compatibility},
		{"level", level},
	};

	return qi;
}
