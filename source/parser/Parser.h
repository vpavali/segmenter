
#pragma once

#include <memory>
#include <json11.hpp>
#include <stream.h>
#include <segmenter/segments.h>

class ParseInfo;

typedef std::shared_ptr<ParseInfo> ParseInfoPtr;
typedef std::vector<ParseInfoPtr> TrackParseInfo;
typedef std::vector<TrackParseInfo> FullParseInfo;

class ParseInfo
{
public:

	uint64_t average_bitrate;
	uint64_t max_bitrate;
	Stream stream;
	std::string lang = "und";

	virtual std::string codec_private_data() const = 0;
	virtual json11::Json quality_info() = 0;
};

class H264ParseInfo: public ParseInfo
{
	typedef std::vector<uint8_t> ByteArray;
public:
	int width;
	int height;
	int profile;
	int level;
	int profile_compatibility;

	std::vector<ByteArray> sps_list;
	std::vector<ByteArray> pps_list;

	std::string codec_private_data() const override;
	virtual json11::Json quality_info() override;
};

class AacParseInfo: public ParseInfo
{
public:
	int sampling_rate = 0;
	int sampling_frequency_index = 0;
	int sample_bit_depth = 16;
	int channels = 0;
	int channels_configuration;
	std::string channels_configuration_str;

	std::string codec_private_data() const override;
	virtual json11::Json quality_info() override;
};

class Parser
{
	TrackParseInfo _parse(MultiSegmentPtr ms);
	ParseInfoPtr _parse(const MonoSegment & ms);
	ParseInfoPtr _parse_h264(const MonoSegment & ms);
	ParseInfoPtr _parse_aac(const MonoSegment & ms);
	ParseInfoPtr _parse_ac3(const MonoSegment & ms);

	void _calc_bitrate(ParseInfoPtr pi, const MonoSegment & ms);
public:
	FullParseInfo parse(MultiTrackSegmentPtr mts);
};

typedef std::shared_ptr<H264ParseInfo> H264ParseInfoPtr;
typedef std::shared_ptr<AacParseInfo> AacParseInfoPtr;
