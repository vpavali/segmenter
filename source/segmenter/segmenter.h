
#ifndef __SEGMENTER_H__
#define __SEGMENTER_H__

#include <map>
#include <spdlog/spdlog.h>
#include <AccessUnit.h>
#include <optional.h>
#include <segmenter/segments.h>
#include <segmenter/SegmentWriter.h>
#include <segmenter/MultiSegmentBuilder.h>

class Segmenter
{
	std::shared_ptr<spdlog::logger> _log;
	uint64_t _mts_index = 0;
	uint64_t _slice_id = 0;
	int _sync_stream;
	std::map<int, MultiSegmentBuilderPtr> _ms_builders;
	Optional<TimeValue> _last_segmenting_point;
	TimeValue _min_segment_duration = TimeValue(3, 2);
	SegmentWriterPtr _segment_writer;

	void _try_to_build_mts();
	void _emit_mts(MultiTrackSegmentPtr mts);
	void _select_sync_stream(const std::map<int, std::vector<Stream>> & adapt_streams);
	void _validate(const std::map<int, std::vector<Stream>> & adapt_streams);
	bool _all_streams_ahead_of(TimeValue sync_point);
public:

	Segmenter(SegmentWriterPtr segment_writer, const std::vector<Stream> & streams);

	void add(AccessUnitPtr au);
};

#endif /*__SEGMENTER_H__*/
