
#include <map>
#include <set>
#include <enforce.h>
#include "segmenter.h"

Segmenter::Segmenter(SegmentWriterPtr segment_writer, const std::vector<Stream> & streams)
:
	_segment_writer(segment_writer)
{
	_log = spdlog::stdout_logger_mt("segmenter");

	std::map<int, std::vector<Stream>> adapt_streams;

	for(auto & st : streams)
	{
		adapt_streams[st.index].push_back(st);
	}

	_validate(adapt_streams);

	for(auto & it: adapt_streams)
	{
		_ms_builders[it.first] = std::make_shared<MultiSegmentBuilder>(it.second);
	}

	_select_sync_stream(adapt_streams);
}

void Segmenter::add(AccessUnitPtr au)
{
	SPDLOG_DEBUG(_log, "added au: stream: {}:{} dts: {} ({}|{:.2f}) pts:{} ({}|{:.2f}) ra: {}", au->stream().input, au->stream().index, au->dts(), rescale_int(au->dts(), SEGMENT_TIME_SCALE), rescale_double(au->dts(), 1), au->pts(), rescale_int(au->pts(), SEGMENT_TIME_SCALE), rescale_double(au->pts(), 1), au->random_access());

	assert(_ms_builders.count(au->stream().index) > 0);

	auto sync_point_found = _ms_builders[au->stream().index]->add(au);

	if(sync_point_found)
	{
		if(au->stream().index == _sync_stream)
		{
			if(!_last_segmenting_point)
			{
				_last_segmenting_point = au->dts();
				SPDLOG_DEBUG(
					_log,
					"first segmenting point: {} ({}|{:.2f})",
					*_last_segmenting_point,
					rescale_int(*_last_segmenting_point, SEGMENT_TIME_SCALE),
					rescale_double(*_last_segmenting_point, 1)
				);
			}

			SPDLOG_DEBUG(
				_log,
				"new sync point in sync stream: {} ({}|{:.2f}) sync_stream.sync_points.size: {}",
				au->dts(),
				rescale_int(au->dts(), SEGMENT_TIME_SCALE),
				rescale_double(au->dts(), 1),
				_ms_builders[_sync_stream]->sync_points().size()
			);
		}

		if(_last_segmenting_point
			&& _ms_builders[_sync_stream]->sync_points().size() > 0
			&& _ms_builders[_sync_stream]->last_sync_point() - *_last_segmenting_point >= _min_segment_duration)
		{
			_try_to_build_mts();
		}
	}
}

void Segmenter::_try_to_build_mts()
{
	auto probe_point = _ms_builders[_sync_stream]->last_sync_point();
	SPDLOG_TRACE(_log, "probe point: {}", rescale_double(probe_point, 1));

	if(_all_streams_ahead_of(probe_point))
	{
		std::vector<MultiSegmentPtr> ms_list;

		for(auto it: _ms_builders)
		{
			auto msb = it.second;
			auto ms = msb->build(probe_point, _mts_index);
			ms_list.push_back(ms);
		}

		auto mts = std::make_shared<MultiTrackSegment>(std::move(ms_list), _slice_id);

		_last_segmenting_point = probe_point;
		_mts_index += 1;

		_emit_mts(mts);
	}
}

void Segmenter::_emit_mts(MultiTrackSegmentPtr mts)
{
	_log->info("built mts #{}: duration {:.2f} time: {}",
				mts->index(),
				rescale_double(mts->duration(), 1),
				rescale_int(mts->time(), SEGMENT_TIME_SCALE)
			);

	_segment_writer->write(mts);
}

void Segmenter::_select_sync_stream(const std::map<int, std::vector<Stream>> & adapt_streams)
{
	_sync_stream = 0;

	for(auto & it: adapt_streams)
	{
		assert(it.second.size() > 0);

		auto s = it.second[0];

		if(s.codec_id == CodecId::H264)
		{
			_sync_stream = s.index;
			break;
		}
	}
}

void Segmenter::_validate(const std::map<int, std::vector<Stream>> & adapt_streams)
{
	int cnt = 0;

	for(auto & it: adapt_streams)
	{
		ENFORCE(it.first == cnt);

		std::set<int> set;
		for(auto st: it.second)
		{
			ENFORCE(set.count(st.input) == 0);
			ENFORCE(st.index == it.second[0].index);

			set.insert(st.input);
		}

		ENFORCE(set.size() == it.second.size());

		cnt += 1;
	}
}

bool Segmenter::_all_streams_ahead_of(TimeValue time_point)
{
	for(auto & it: _ms_builders)
	{
		auto msb = it.second;

		if(msb->sync_points().empty() || msb->last_sync_point() < time_point)
		{
			return false;
		}
	}

	return true;
}
