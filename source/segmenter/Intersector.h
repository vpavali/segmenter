
#ifndef __INTERSECTOR_H__
#define __INTERSECTOR_H__

#include <algorithm>
#include <vector>
#include <map>

template<typename Value>
class Intersector
{
	std::vector<Optional<Value>> _state;
	std::map<Value, size_t> _frequencies;
	std::vector<Value> _isect;

public:
	Intersector(size_t sets_num)
	{
		_state.resize(sets_num);
	}

	bool intersect(size_t idx, Value v)
	{
		_state[idx] = v;
		_frequencies[v] += 1;

		bool r = _frequencies[v] == _state.size();

		if(r)
		{
			_isect.push_back(v);
		}

		return r;
	}

	const std::vector<Value> & intersection() const
	{
		return _isect;
	}

	void remove_less(Value upper_bound)
	{
		auto it = _frequencies.begin();

		while(it != _frequencies.end())
		{
			if(it->first < upper_bound)
			{
				it = _frequencies.erase(it);
			}
			else
			{
				++it;
			}
		}

		auto remove_it = std::find_if(_isect.begin(), _isect.end(), [upper_bound](const Value & x){
			return x >= upper_bound;
		});

		_isect.erase(_isect.begin(), remove_it);
	}
};

#endif /*__INTERSECTOR_H__*/
