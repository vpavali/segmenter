
#ifndef __MULTI_TRACK_SEGMENT_H__
#define __MULTI_TRACK_SEGMENT_H__

#include <memory>
#include <timings.h>
#include <vector>
#include <segmenter/MultiSegment.h>

class MultiTrackSegment
{
	uint64_t _slice_id;
	std::vector<MultiSegmentPtr> _ms_list;
public:

	MultiTrackSegment(std::vector<MultiSegmentPtr> && ms_list, uint64_t slice_id):
		_ms_list(std::move(ms_list))
	{
		_slice_id = slice_id;

#if _DEBUG
		validate();
#endif
	}

	TimeValue time()
	{
		return _ms_list[0]->time();
	}

	Duration duration()
	{
		return _ms_list[0]->duration();
	}

	uint64_t index()
	{
		return _ms_list[0]->index();
	}

	uint64_t slice_id()
	{
		return _slice_id;
	}

	const std::vector<MultiSegmentPtr> segments() const
	{
		return _ms_list;
	}

	void validate()
	{
		for(auto ms: _ms_list)
		{
			ENFORCE(ms->index() == _ms_list[0]->index());
		}
	}
};

typedef std::shared_ptr<MultiTrackSegment> MultiTrackSegmentPtr;

#endif /*__MULTI_TRACK_SEGMENT_H__*/
