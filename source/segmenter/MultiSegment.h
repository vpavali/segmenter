
#ifndef __MULTI_SEGMENT_H__
#define __MULTI_SEGMENT_H__

#include <memory>
#include <enforce.h>
#include <timings.h>
#include <segmenter/MonoSegment.h>

class MultiSegment
{
	bool _validate();

	TimeValue _start_time;
	Duration _duration;
	std::vector<MonoSegment> _mono_segments;
public:

	MultiSegment(TimeValue start_time, Duration duration, std::vector<MonoSegment> && segments)
	:
		_mono_segments(std::move(segments))
	{
		_start_time = start_time;
		_duration = duration;

		assert(_validate());
	}

	TimeValue time()
	{
		return _start_time;
	}

	Duration duration()
	{
		return _duration;
	}

	uint64_t stream_index()
	{
		return _mono_segments[0].stream().index;
	}

	const std::vector<MonoSegment> & segments() const
	{
		return _mono_segments;
	}

	uint64_t index() const
	{
		return _mono_segments[0].index;
	}
};

typedef std::shared_ptr<MultiSegment> MultiSegmentPtr;

inline bool MultiSegment::_validate()
{
	ENFORCE(_mono_segments.size() > 0);

	for(auto & monoseg: _mono_segments)
	{
		ENFORCE(monoseg.stream().index == _mono_segments[0].stream().index);
		ENFORCE(monoseg.index == _mono_segments[0].index);

		monoseg.validate();
	}

	return true;
}

#endif /*__MULTI_SEGMENT_H__*/
