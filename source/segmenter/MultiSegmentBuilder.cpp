
#include <enforce.h>
#include <segmenter/SegmentWriter.h>
#include "MultiSegmentBuilder.h"

MultiSegmentBuilder::MultiSegmentBuilder(const std::vector<Stream> & streams)
:
	_intersector(streams.size())
{
	_samples.resize(streams.size());
	_stream_index = streams[0].index;
	_log = spdlog::stdout_logger_mt(fmt::format("msb:{}", _stream_index));
}

bool MultiSegmentBuilder::add(AccessUnitPtr au)
{
	assert(au->stream().index == _stream_index);

	if(!_first_ra_found)
	{
		if(! au->random_access())
		{
			_log->warn("skipping au until first sync point from stream: {}:{} dts: {} ({}|{:.2f}) pts:{} ({}|{:.2f}) ra: {}", au->stream().input, au->stream().index, au->dts(), rescale_int(au->dts(), SEGMENT_TIME_SCALE), rescale_double(au->dts(), 1), au->pts(), rescale_int(au->pts(), SEGMENT_TIME_SCALE), rescale_double(au->pts(), 1), au->random_access());

			return false;
		}
		else
		{
			_first_ra_found = true;
		}
	}

	assert(_first_ra_found);

	_samples[au->stream().input].push_back(au);

	if(au->random_access())
	{
		auto new_sync_found = _intersector.intersect(au->stream().input, au->dts());

		return new_sync_found;
	}

	return false;
}

MultiSegmentPtr MultiSegmentBuilder::build(TimeValue sync_point, uint64_t index)
{
	std::vector<MonoSegment> mono_segments;

	TimeValue start_time;

	for(auto & mono_samples: _samples)
	{
		ENFORCE(mono_samples.size() > 0);
		ENFORCE(mono_samples.back()->dts() >= sync_point);

		start_time = mono_samples[0]->dts();
		auto it = mono_samples.begin();

		while(it != mono_samples.end() && (*it)->dts() < sync_point)
		{
			++it;
		}

		auto mono_segment_samples = std::vector<AccessUnitPtr>(mono_samples.begin(), it);
		auto mono_segment = MonoSegment(index, start_time, sync_point - start_time, std::move(mono_segment_samples));
		mono_segments.emplace_back(std::move(mono_segment));
		mono_samples.erase(mono_samples.begin(), it);
		_intersector.remove_less(sync_point);

		SPDLOG_TRACE(_log, "mono_samples.size = {}", mono_samples.size());
		SPDLOG_TRACE(_log, "mono_segments.back.size = {}", mono_segments.back().size());

		ENFORCE(mono_samples.size() == 0 || mono_samples.front()->dts() >= sync_point);
	}

	return std::make_shared<MultiSegment>(start_time, sync_point - start_time, std::move(mono_segments));
}

TimeValue MultiSegmentBuilder::last_sync_point() const
{
	ENFORCE(_intersector.intersection().size() > 0);

	return _intersector.intersection().back();
}

const std::vector<TimeValue> & MultiSegmentBuilder::sync_points() const
{
	return _intersector.intersection();
}
