
#include <enforce.h>
#include "File.h"

File::File(boost::filesystem::path path, const char * mode)
{
	_path = path;
	_fd = fopen(path.c_str(), mode);

	enforce_ex<IoException>(_fd != nullptr, fmt::format("failed to open file {} in mode '{}'", path, mode));
}

File::~File()
{
	fclose(_fd);
}

void File::write(const uint8_t * data, size_t size)
{
	auto sz = fwrite(data, 1, size, _fd);

	enforce_ex<IoException>(sz == size, fmt::format("failed to write to file {}: only {} bytes written out of {} requested", _path, sz, size));
}

void File::write(const char * data, size_t size)
{
	write((const uint8_t*) data, size);
}

void File::write(const std::string & s)
{
	write(s.data(), s.size());
}

void File::writeln(const std::string & s)
{
	write(s);
	write("\n", 1);
}
