
#include <stdexcept>
#include <string>
#include <cassert>
#include "stream.h"

MediaType Stream::media_type() const
{
	return codec_to_media_type(codec_id);
}

CodecId stream_id_to_codec_id(uint8_t stream_type)
{
    switch (stream_type) {
        case 0x0F: return CodecId::Aac;
        case 0x1B: return CodecId::H264;
        default  : throw std::runtime_error("unknown stream type " + std::to_string(stream_type));
	}
}

MediaType codec_to_media_type(CodecId codec_id)
{
	switch(codec_id)
	{
		case CodecId::H264: return MediaType::Video;
		case CodecId::Aac: return MediaType::Audio;
		case CodecId::Ac3: return MediaType::Audio;
	}
}

namespace std
{
	string to_string(MediaType mt)
	{
		switch(mt)
		{
			case MediaType::Video: return "video";
			case MediaType::Audio: return "audio";
		}

		assert(false);
	}
}

std::ostream & operator<<(std::ostream & os, CodecId c)
{
	switch(c)
	{
		case CodecId::H264: os << "h264";
		case CodecId::Aac: os << "aac";
		case CodecId::Ac3: os << "ac3";

		default: os << "unknown code: " + std::to_string((int) c);
	}

	return os;
}
