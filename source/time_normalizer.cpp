
#include <segmenter/SegmentWriter.h>
#include "time_normalizer.h"

TimeNormalizer::TimeNormalizer(const std::vector<Stream> & streams)
{
	for(auto & st: streams)
	{
		auto key = std::make_pair(st.input, st.index);

		_samples[key] = SamplesQueue();
	}

	assert(_samples.size() == streams.size());
}

AccessUnitPtr TimeNormalizer::normalize(AccessUnitPtr au)
{
	auto key = std::make_pair(au->stream().input, au->stream().index);

	ENFORCE(_samples.count(key) > 0);

	_samples[key].push(au);

	if(!_first_dts)
	{
		_calc_first_dts();
	}

	if(!_first_dts)
	{
		return nullptr;
	}

	au = _samples[key].front();
	_samples[key].pop();

	au->shift_time(*_first_dts);

	return au;
}

void TimeNormalizer::_calc_first_dts()
{
	assert(!_first_dts);

	auto min_dts = TimeValue(INT64_MAX, 1);

	for(auto it: _samples)
	{
		if(it.second.empty())
		{
			return;
		}

		min_dts = std::min(min_dts, it.second.front()->dts());
	}

	_first_dts = min_dts;

	spdlog::get("app")->info("first dts: {} ({}|{:.2f})",
				*_first_dts,
				rescale_int(*_first_dts, SEGMENT_TIME_SCALE),
				rescale_double(*_first_dts, 1)
	);
}
