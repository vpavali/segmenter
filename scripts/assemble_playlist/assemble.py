#!/usr/bin/env python

import argparse
import copy
import json
import os
import os.path
import collections
import mako.template
import shutil

AUDIO_TAGS = {
    "aac": 255,
    "eac3": 65534,
    "ac3": 65534,
}

FOURCC = {
    "h264": "H264",
    "aac": "AACL",
    "ac3": "EC-3",
    "eac3": "EC-3",
}

class App(object):

    def __init__(self):

        self.handlers = collections.defaultdict(lambda: self.skip_ext_print)
        self.handlers['.push'] = self.skip_ext
        self.handlers['.data'] = self.skip_ext
        self.handlers['.meta'] = self.handle_meta
        self.handlers['.slice'] = self.handle_slice
        self.segments = collections.defaultdict(list)
        self.init_segments = {}

    def skip_ext(self, path):
        pass

    def skip_ext_print(self, path):

        print 'skipping {}', path

    def handle_meta(self, path):

        m = json.load(open(self.fullpath(path)))
        tid = m['track_id']

        if m['type'] == 'data':
            self.segments[tid].append(m)
        elif m['type'] == 'init':
            self.init_segments[tid] = m
        else:
            raise Exception('unknown segment type {}'.format(m['type']))

    def handle_slice(self, path):
        j = json.load(open(self.fullpath(path)))

        self.slice = copy.deepcopy(j)
        self.slice['tracks'] = {}
        del self.slice['track_list']
        for t in j['track_list']:

            if self.options.only_video and t['media_type'] != 'video':
                print 'skipping non video track {}: {}'.format(t['track_id'], t['media_type'])
                continue

            t1 = copy.deepcopy(t)
            del t1['quality_list']

            t1['qualities'] = {}
            for q in t['quality_list']:
                qid = q['quality_id']
                if q['codec'] == 'h264':
                    q['codec_ext'] = 'avc1.{:02x}{:02x}{:02x}'.format(
                        q['profile'],
                        q['profile_compatibility'],
                        q['level'],
                    )
                t1['qualities'][qid] = q

            tid = t['track_id']
            self.slice['tracks'][tid] = t1

        # print 'slice:'
        # print json.dumps(self.slice, indent=4)

    def fullpath(self, path):
        return os.path.join(self.segs_dir, path)

    def fulldst(self, path):
        return os.path.join(self.outdir, path)

    def parse_args(self):

        p = argparse.ArgumentParser()
        p.add_argument('-b', '--rewrite-base', dest='rewrite_base', default='')
        p.add_argument('-t', '--type', dest='type', required=True, help='output type: dash|smooth|mp4')
        p.add_argument('--only-video', dest='only_video', type=bool, default=False)
        p.add_argument('segsdir')
        p.add_argument('outdir')
        self.options = p.parse_args()

        self.segs_dir = self.options.segsdir
        self.outdir = self.options.outdir
        self.type = self.options.type

        assert self.type in ['dash', 'smooth', 'mp4']

        assembler_factory = {
            'smooth': SmoothAssembler,
            'dash': DashAssembler,
        }

        self.assembler = assembler_factory[self.type](self.segs_dir, self.outdir, self.options.rewrite_base)

    def run(self):

        self.parse_args()
        self.parse_segments()

        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)

        self.assembler.assemble(self.slice, self.segments, self.init_segments)

    def parse_segments(self):
        for path in os.listdir(self.segs_dir):

            _, ext = os.path.splitext(path)

            self.handlers[ext](path)

        for tid in self.segments.keys():
            self.segments[tid] = sorted(self.segments[tid], key=lambda x: x['index'])

class SmoothAssembler(object):

    def __init__(self, input_, outdir, rewrite_base):

        self.input_ = input_
        self.outdir = outdir
        self.rewrite_base = rewrite_base

    def fullsrc(self, path):
        return os.path.join(self.input_, path)

    def fulldst(self, path):
        return os.path.join(self.outdir, path)

    def assemble_manifest(self, slice_, segments):

        template_path = os.path.join(os.path.dirname(__file__), 'manifest.mako')
        template = mako.template.Template(filename=template_path)
        manifest = template.render(slice_=slice_, segments=segments, AUDIO_TAGS=AUDIO_TAGS, FOURCC=FOURCC)

        manifest_path = os.path.join(self.outdir, 'manifest.xml')
        with open(manifest_path, 'w') as fd:
            fd.write(manifest)

    def assemble_htaccess(self):
        template_path = os.path.join(os.path.dirname(__file__), 'htaccess.mako')
        template = mako.template.Template(filename=template_path)
        content = template.render(rewrite_base=self.rewrite_base)

        dst = self.fulldst('.htaccess')

        with open(dst, 'w') as fd:
            fd.write(content)

    def copy_segments(self, segments):

        if not os.path.exists(self.fulldst('segs')):
            os.makedirs(self.fulldst('segs'))

        for tid in segments.keys():
            for s in segments[tid]:
                src = '{}.data'.format(s['segment_id'])
                dst = 'segs/{}-{}-{}.m4f'.format(s['track_id'], s['quality_id'], s['time'])
                shutil.copyfile(self.fullsrc(src), self.fulldst(dst))

    def assemble(self, slice_, segments, init_segments):

        self.assemble_manifest(slice_, segments, )
        self.assemble_htaccess()
        self.copy_segments(segments)

class DashAssembler(object):

    def __init__(self, input_, outdir, rewrite_base):

        self.input_ = input_
        self.outdir = outdir
        self.rewrite_base = rewrite_base

    def fullsrc(self, path):
        return os.path.join(self.input_, path)

    def fulldst(self, path):
        return os.path.join(self.outdir, path)

    def assemble_manifest(self, slice_, segments):

        template_path = os.path.join(os.path.dirname(__file__), 'dash.mako')
        template = mako.template.Template(filename=template_path)
        manifest = template.render(slice_=slice_, segments=segments)

        manifest_path = self.fulldst('manifest.xml')
        with open(manifest_path, 'w') as fd:
            fd.write(manifest)

    def assemble_htaccess(self):
        template_path = os.path.join(os.path.dirname(__file__), 'htaccess.mako')
        template = mako.template.Template(filename=template_path)
        content = template.render(rewrite_base=self.rewrite_base)

        dst = self.fulldst('.htaccess')

        with open(dst, 'w') as fd:
            fd.write(content)

    def copy_segments(self, segments, init_segments):

        if not os.path.exists(self.fulldst('segs')):
            os.makedirs(self.fulldst('segs'))

        for tid in segments.keys():

            s = init_segments[tid]

            src = '{}.data'.format(s['segment_id'])
            dst = 'segs/{}-{}-init.m4f'.format(s['track_id'], s['quality_id'])
            shutil.copyfile(self.fullsrc(src), self.fulldst(dst))

            for s in segments[tid]:
                src = '{}.data'.format(s['segment_id'])
                dst = 'segs/{}-{}-{}.m4f'.format(s['track_id'], s['quality_id'], s['index'])
                shutil.copyfile(self.fullsrc(src), self.fulldst(dst))

    def assemble(self, slice_, segments, init_segments):

        self.assemble_manifest(slice_, segments, )
        self.assemble_htaccess()
        self.copy_segments(segments, init_segments)


app = App()
app.run()
