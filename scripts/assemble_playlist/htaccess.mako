RewriteEngine On

RewriteBase /${rewrite_base}

Header set Access-Control-Allow-Origin "*"

RewriteRule   ^manifest$ manifest.xml  [T=text/xml]
RewriteRule   ^manifest.mpd$ manifest.xml  [T=text/xml]
RewriteRule   ^QualityLevels\((\d+)\)/Fragments\(track(\d)=(\d+)\)$ segs/$2-$1-$3.m4f  [T=video/mp4]
