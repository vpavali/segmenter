
ExternalProject_Add(
	bitstream

	PREFIX ${CMAKE_BINARY_DIR}/externals/bitstream/

	GIT_REPOSITORY git://git.videolan.org/bitstream.git
	GIT_TAG f97204694230063c57c0988e693e395489e7b707

	CONFIGURE_COMMAND ""

	# PATCH_COMMAND git apply "${CMAKE_SOURCE_DIR}/cmake/bitstream.patch"

	BUILD_COMMAND ""
	BUILD_IN_SOURCE 1

	INSTALL_COMMAND make install PREFIX=${CMAKE_BINARY_DIR}/headers/bitstream/
)
